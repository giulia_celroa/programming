import csv, sys, re, os
import networkx as nx
import matplotlib.pyplot as plt
import datetime


def main():
	now_str = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
	# 1.2.4
	path = sys.argv[1]
	files = []
	if os.path.isdir(path):
		files = [os.path.join(path, fname) for fname in os.listdir(path) if not (fname.startswith(".") or fname.endswith("~"))]
	else:
		files.append(path)

	for path in files:
		organism_name = re.findall("BIOGRID-ORGANISM-.+-[0-9]\.[0-9]\.[0-9]+", path)[0]
		print("Processing: ", organism_name)	

		outdir = os.path.join("output", now_str , organism_name)
		if not os.path.exists(outdir):
			os.makedirs(outdir)

		edgelist_path = os.path.join(outdir, organism_name + ".edgeList")
		symbol_path = os.path.join(outdir, organism_name + ".proteinSymbols")

		try:
			generate_network(path, edgelist_path, symbol_path)
	
		except IOError:
			print("Input file does not exist (", path, ")")

"""
Generate the edge list and symbol list for a BIOGRID network file in either tab or tab2 format.
	@param path The path to the BIOGRID file.
	@param edgelist_path The path for saving the edge list.
	@param symbol_path The path for saving the symbol list.

	@returns None. Files are written to edgelist_path and symbol_path. The edge list is a comma seperated value file where every line "a, b" denotes a connection from a to b. The symbol list is a comma seperated value file where every line "x, y" denotes that y is the official symbol for the protein with ID X.
"""
def generate_network(path, edgelist_path, symbol_path):
	#interactome = nx.Graph()
	ids_to_symbols = {}
	#1.2.5
	parallel_edge_check = set()
	if "tab2" in path: # Tab 2 format
		BIOGRID_NAME_A = 3
		BIOGRID_NAME_B = 4
		OFFICIAL_SYMBOL_A = 7
		OFFICIAL_SYMBOL_B = 8
	else: # Tab 1 format.
		BIOGRID_NAME_A = 0
		BIOGRID_NAME_B = 1
		OFFICIAL_SYMBOL_A = 2
		OFFICIAL_SYMBOL_B = 3
	with open(path, "rb") as file:
		with open(edgelist_path, "wb") as edgefile:
			reader = csv.reader(file, delimiter="\t", quotechar="\"")
			next(reader, None) # Skip header.
			for row in reader:
				#print(row[BIOGRID_NAME_A], row[BIOGRID_NAME_B], row[OFFICIAL_SYMBOL_A], row[OFFICIAL_SYMBOL_B])
					# Remove Parallel Edges & Self Loops
				if not ((row[BIOGRID_NAME_A], row[BIOGRID_NAME_B]) in parallel_edge_check or \
					(row[BIOGRID_NAME_B], row[BIOGRID_NAME_A]) in parallel_edge_check or \
					row[BIOGRID_NAME_A] == row[BIOGRID_NAME_B]):

					# Mark edges so skipped in future.
					parallel_edge_check.add((row[BIOGRID_NAME_A], row[BIOGRID_NAME_B]))
					parallel_edge_check.add((row[BIOGRID_NAME_B], row[BIOGRID_NAME_A]))

					#...
					ids_to_symbols[row[BIOGRID_NAME_A]] = row[OFFICIAL_SYMBOL_A]
					ids_to_symbols[row[BIOGRID_NAME_B]] = row[OFFICIAL_SYMBOL_B]
					edgefile.write(row[BIOGRID_NAME_A] + ", " + row[BIOGRID_NAME_B] + "\n")

			
					# 1.2.3
					#interactome.add_node(row[BIOGRID_NAME_A], {"symbol": row[OFFICIAL_SYMBOL_A] })
					#interactome.add_node(row[BIOGRID_NAME_B], {"symbol": row[OFFICIAL_SYMBOL_B] })
					#interactome.add_edge(row[BIOGRID_NAME_A], row[BIOGRID_NAME_B])

	with open(symbol_path, "wb") as symbolfile:
		for key, value in ids_to_symbols.items():
			symbolfile.write(key + ", " + value + "\n")

if __name__ == "__main__":
	main()
	
