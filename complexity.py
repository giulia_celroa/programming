import timeit, functools
from gennets import generate_network
from network import build_graph
import os, glob, re, sys, csv
import numpy as np

# Fix Timeit to also return value
"""
A timer function for use with the Timer class which is invoked if the Timer class is passed a callable. Used to cause the return value to be returned along with the time.
	@param setup The function to be run _before_ the function that you will be timed.
	@param func The function to be timed.
	
	@return A closure which when invoked returns the time to run `func` and it's return value.
"""
def _template_func(setup, func):
    """Create a timer function. Used if the "statement" is a callable."""
    def inner(_it, _timer, _func=func):
        setup()
        _t0 = _timer()
        for _i in _it:
            retval = _func()
        _t1 = _timer()
        return _t1 - _t0, retval
    return inner

timeit._template_func = _template_func

path = sys.argv[1]
files = glob.glob(os.path.join(path, "*"))

edgelist_path = os.path.join(os.sep, "tmp", "tmp.edgeList")
symbol_path = os.path.join(os.sep, "tmp", "tmp.proteinSymbols")

time_graphs = []
time_datas = []
nodess = []
edgess = []

with open('complexity_data.csv', 'wb') as csvfile:
	writer = csv.writer(csvfile, delimiter=',',
                            quotechar='\"', quoting=csv.QUOTE_MINIMAL)
	writer.writerow(["organism_name", "time_data", "time_graph", "nodes", "edges"])

	for file in files:
		sys.exit()
		organism_name = re.findall("BIOGRID-ORGANISM-.+-[0-9]\.[0-9]",file)[0]
		print("Processing: ", organism_name)
		timer_process_data = timeit.Timer(functools.partial(generate_network, file, edgelist_path, symbol_path))
		timer_build_graph = timeit.Timer(functools.partial(build_graph, edgelist_path, symbol_path))
		time_data, _ = timer_process_data.timeit(1)
		time_graph, graph = timer_build_graph.timeit(1)
		nodes = len(graph.nodes())
		edges = len(graph.edges())
		print(time_data)
		print(time_graph, nodes, edges)
		writer.writerow([organism_name, time_data, time_graph, nodes, edges])
		#time_graphs.append(time_graph)
		#time_datas.append(time_datas)
		#nodess.append(nodes)
		#edgess.append(edges)

