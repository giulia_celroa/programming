from networkx import *
from random import randint
import pdb 
import time
import numpy as np 
import sys, csv


class box_Graph(Graph):
	"""
	A new class to represent a tree of boxes, the basic structure is a weighted tree.
	The weight is equal to the maximum shortest path between any nodes in one box and the other
	"""

	def __init__(self,G,distances=None):
		Graph.__init__(self,G.copy())
		self.G = G
		if distances == None:
			self.distances = dict(all_pairs_shortest_path_length(G))
			
		else:
			self.distances = distances

		for node in G.nodes():
			self.nodes[node]['subset'] = set([node])
			for connect_node in [x for x in G.adj[node] if x> node]:
				self[node][connect_node]['weight'] = 1
		


	def update(self,old_nodes):
		"""
		This routine takes care of merging two boxes and updating the box graph accordingly:
		Input: vector with the indeces of the two boxes that needs to be merged
		"""
		
		check = set(self.adj[old_nodes[1]]).union(set(self.adj[old_nodes[0]]))-set(old_nodes)
		
		box = self.nodes[old_nodes[1]]['subset'].union(self.nodes[old_nodes[0]]['subset'])
		
		for k in [x for x in check]:
			new_weight = 0
			for k_node in self.nodes[k]['subset']:
				temp_weight = max([self.distances[k_node][x] for x in box])
				if temp_weight > new_weight:
					new_weight = temp_weight
					connection = k_node
			
			try:
				self.edges[old_nodes[0],k]['weight']= new_weight
			except:
				self.add_weighted_edges_from([(old_nodes[0],k,new_weight)])

			self.edges[old_nodes[0],k]['connection']=connection
		
		self.nodes[old_nodes[0]]['subset'] = self.nodes[old_nodes[0]]['subset'].union(self.nodes[old_nodes[1]]['subset'])
		
		self.remove_node(old_nodes[1])
		

	def capacity(self):
		"""
		The capity of a box is equal to the number of nodes it contains
		"""
		return [len(self.nodes[node]['subset']) for node in self.nodes]

	def neighbours(self,centre,l):
		return [node for node in self.adj[centre] if self[centre][node]['weight']<=l]
	
	def divisible(self,centre,L):
		"""
		Input
		- centre: index of the box of interest
		- L: size of the box 

		If possible, this function splits the box (centre) into the adjacent boxes
		"""
		
		old_nodes = set(self.nodes[centre]['subset'])
		check = list(dict(self.adj[centre]).keys())
		update = {}

		if len(check)==1:
			return

		for new_centre in check:
			box = self.nodes[new_centre]['subset']
			include = set([new for new in old_nodes if max([self.distances[new][x] for x in box])<=L])
			old_nodes -= include
			if len(include)>0:
				update[new_centre] = list(include) 

		if not old_nodes:
			
			self.remove_node(centre)
						
			for box_update in update.keys():
				
				self.add_node(update[box_update][0])
				self.nodes[update[box_update][0]]['subset']= set(update[box_update])
				new_connection = []
				for y in update[box_update]:
					new_connection += [(box_connect,update[box_update][0],1) for box_connect in check if min([self.distances[y][k] for k in self.nodes[box_connect]['subset']])==1] 
				self.add_weighted_edges_from(new_connection)
				check += [update[box_update][0]]

			for box_update in update.keys(): 
				self.update([box_update,update[box_update][0]])

	def modularity(self):
		"""
		This function evaluates the modularity measure based on the current boxes
		"""
		N = len(self.nodes)
		mod = 0	
		for node in self.nodes():
			L_in = 0 
			L_out = 0
			in_nodes = list(self.nodes[node]['subset'])
			
			for j in in_nodes:
				temp = len([1 for x in self.G.adj[j] if x in in_nodes])
				L_in += temp/2
				L_out += len(self.G.adj[j])-temp
			mod += L_in/L_out 
		return mod/N
				
				
				

def box_covering(G_reduce, size, algorithm="modified"):
	"""
	This is the main function with the main body of the covering algorithm described in the report.
	Input:
	- a box graph 
	- the size of the original network G
	- algorithm: by default the function use the modified version of the covering algorithm presented in our report, if differently specified it use the original 		version in Concas et al.
	Output: 
	- vector with the number of boxes and modularity ratio corresponding to different size 
	"""
	
	N_covering=[size]
	M_covering=[]
	
	j=1
	while (j==1) or (N_covering[-1]>1): 
		# compute the modularity ratio
		M_covering.append(G_reduce.modularity())	
		
		                                            
		unburned_node = list(G_reduce.nodes())
		size = len(unburned_node)
		
		# to explore the nodes we use the same ordering proposed by Concas et al.		
		if j == 1:
			capacity_node = dict(G_reduce.degree())
			capacity = list(set([capacity_node[x] for x in capacity_node.keys()]))
		else:
			capacity = G_reduce.capacity()
			capacity_node = dict(zip(G_reduce.nodes(),capacity))
			capacity = list(set(capacity))
		
		
		minimum_capacity = min(capacity)
		
		capacity.remove(minimum_capacity)
		list_centre = [node for node in unburned_node if capacity_node[node]==minimum_capacity]
		
		while unburned_node:
	
			centre = list_centre.pop(randint(0, len(list_centre)-1))
			unburned_node.remove(centre)

			neigh = G_reduce.neighbours(centre,j)
			
			
			if (algorithm=="modified") and (not neigh) and (N_covering[-1]>2):
				 G_reduce.divisible(centre,j)
				
			while neigh:

				node = neigh.pop(randint(0, len(adjacent)-1))
				G_reduce.update([centre,node])
				# we need to update the list of neighbouring boxes, since this can be affected by the merging operation 
				neigh = G_reduce.neighbours(centre,j)

				# we burn the node/box merged with the centre so that it is not considered again
				try:
					unburned_node.remove(node)
				except: 
					pass

				try:
					list_centre.remove(node)
				except:
					pass
			
			# once the node has no more neighbours we move to explore another node/box
			while not(list_centre) and (capacity):
				minimum_capacity = min(capacity)
				capacity.remove(minimum_capacity)
				list_centre = [node for node in unburned_node if capacity_node[node]==minimum_capacity]
		
		N_covering.append(len(G_reduce.nodes))
		
		j=j+1

	return (N_covering,M_covering)

def tfd_burning(G, G_reduce,iterations=5, name="UNKNOWN"): #, lmax)
	"""
	The function receive as an input the original graph and corresponding box graph. 
	This performe the specified number of iterations and save the corresponding result in a txt file. 
	If not specified the file is called UNKNOWN by default 
	"""
	N_min = None
	
	
	F  = open(name + '.Nmatrix.txt', 'w')
	N_min = None
	F.write('N = [')

	for i in range(iterations):

		G_secondary = box_Graph(G,distances=G_reduce.distances)
		start = time.time()
		temp = box_covering(G_secondary,len(G.nodes()))
		end = time.time()


		F.write(','.join([str(n) for n in temp[0]])+';\n')


		if N_min is None:
			N_min = temp[0]
			M_average = np.array(temp[1])
		else:
			N_min = [min(N_min[j],temp[0][j]) for j in range(len(N_min))]   
			M_average += np.array(temp[1])

	M_average = M_average/float(iterations)
	F.write('N_min=['+','.join([str(n) for n in N_min])+'];\n')
	F.write('M_average=['+','.join([str(n) for n in M_average])+'];\n')
	F.close()
				
