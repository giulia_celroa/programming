from __future__ import division
from network import build_graph, symbolify
import sys, os, re
import networkx as nx
import operator
from matplotlib import pyplot as plt
from dist_colour import greedy_tfd
import numpy as np

# Random graph for testing: fast_gnp_random_graph(nodes, probability of edge creation)

path = sys.argv[1]

ppins = [os.path.join(path, file) for file in sorted(os.listdir(path))] # Requires files be named correctly for chronological order.

graphs = []

"""
Returns the largest connected component.
	@param The graph from which the largest connected component is extracted. This is the subgraph which has the most nodes.

	@return A NetworkX graph object reperesenting the largest connected component in graph.
"""
def lcc(graph):
	return max(nx.connected_component_subgraphs(graph), key=len)

"""
Returns the officical name of the central protein in the graph determined using the supplied centraility method.
	@param graph The graph on which to compute the centrality metric.
	@param centrality_method The method used to compute the centrality of each node, e.g. betweenness/degree/etc. centrality.

	@return The official symbol assocated with the most central protein in the graph.
"""
def central_protein(graph, centrality_method):
	pid = max(centrality_method(graph).iteritems(), key=operator.itemgetter(1))[0] # (key, value) => key
	return symbolify(pid, graph)	

"""
Compute the mean value of dictionaries values which are numeric.
	@param dic The dictionary of * -> numeric pairs for which the mean value of "numeric" is computed.

	@return The mean value in the dictionary.
"""
def mean_dict_value(dic):
	acc = 0
	count = 0
	for _, value in dic.items():
		acc += value
		count += 1
	return acc / count

for ppin in ppins:
	organism_name = re.findall("BIOGRID-ORGANISM-.+-[0-9]\.[0-9]\.[0-9]+", ppin)[0]
	print("Processing: ", organism_name)	

	edgelist_path = os.path.join(ppin, organism_name + ".edgeList")
	symbol_path = os.path.join(ppin, organism_name + ".proteinSymbols")
	
	graphs.append(build_graph(edgelist_path, symbol_path))

times = [str(t) for t in range(2006, 2018 + 1)]
#edges = [len(graph.edges()) for graph in graphs]
#nodes = [len(graph.nodes()) for graph in graphs]
#lcc_edges = [len(lcc(graph).edges()) for graph in graphs]
#lcc_nodes = [len(lcc(graph).nodes()) for graph in graphs]
#central_protein = [central_protein(graph, nx.degree_centrality) for graph in graphs]
#clustering_coefficient = [nx.average_clustering(graph) for graph in graphs]
#mean_degree = [mean_dict_value(nx.degree_centrality(graph)) for graph in graphs]
density = [nx.density(graph) for graph in graphs]
#tfd = [greedy_tfd(graph) for graphs in graph]

#diameter = [nx.diameter(graph) for graph in graphs]
#radius = [nx.radius(graph) for graph in graphs]

"""
print(edges)
np.save("ot_edges.npy", edges)
print(nodes)
np.save("ot_nodes.npy", edges)
print(lcc_edges)
np.save("ot_lcc_edges.npy", lcc_edges)
print(lcc_nodes)
np.save("ot_lcc_nodes.npy", lcc_nodes)
print(central_protein)
np.save("ot_central_protein.npy", central_protein)
print(mean_degree)
np.save("ot_mean_deg.npy", mean_degree)
print(density)

np.save("ot_density.npy", density)

fig = plt.figure()
ax = fig.add_subplot(111)
plt.title("Nodes & Edges (with largest connected component) Evolution")
plt.xlabel("Year")
plt.ylabel("Count")
ax.plot(times, nodes, label="nodes")
ax.plot(times, edges, label="edges")
ax.plot(times, lcc_edges, label="LCC Edges")
ax.plot(times, lcc_nodes, label="LCC Nodes")
ax.legend()
fig.tight_layout()
plt.show()

fig = plt.figure()
ax = fig.add_subplot(111)
plt.title("Clustering Coefficient Evolution")
plt.xlabel("Year")
plt.ylabel("Clustering Coefficient")
ax.plot(times, clustering_coefficient)
ax.legend()
fig.tight_layout()
plt.show()

fig = plt.figure()
ax = fig.add_subplot(111)
plt.title("Mean Degree Evolution")
plt.xlabel("Year")
plt.ylabel("Mean Degree")
ax.plot(times, mean_degree)
ax.legend()
fig.tight_layout()
plt.show()
"""
fig = plt.figure()
ax = fig.add_subplot(111)
plt.title("Density Evolution")
plt.xlabel("Year")
plt.ylabel("Density")
ax.plot(times, density)
ax.legend()
fig.tight_layout()
plt.show()
