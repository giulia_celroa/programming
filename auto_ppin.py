import networkx as nx
import os, sys, glob, csv, re
from dist_colour import greedy_tfd, _diameter
from network import build_graph
from covering import box_covering, box_Graph, tfd_burning
import numpy as np
#'import matplotlib.pyplot as plt

path = sys.argv[1]
files = sorted(glob.glob(os.path.join(path, "*")))

def nested_dict_max(dic):
    m = 0
    for _, d1 in dic.items():
        for k2, v in d1.items():
            if v > m:
                m = v
    return m

def graphsize(g):
	return len(g.nodes())

with open('evodyn_greedy3.csv', 'a+') as csvfile:
	writer = csv.writer(csvfile, delimiter=',',
                            quotechar='\"', quoting=csv.QUOTE_MINIMAL)
	writer.writerow(["organism_name", "tfd"])
	
	for file in files:
		try:
			#organism_name = re.findall("BIOGRID-ORGANISM-.+-[0-9]\.[0-9]\.[0-9]+",file)[0]		
			organism_name = re.findall("BIOGRID-ORGANISM-.+-[0-9]\.[0-9]\.[0-9]+", file)[0]
		except IndexError: # The folder doesn't match the name pattern!
			continue
		print("Processing: ", organism_name)
		
		edgelist_path = os.path.join(file, organism_name + ".edgeList")
		symbol_path = os.path.join(file, organism_name + ".proteinSymbols")

		interactome = build_graph(edgelist_path, symbol_path)
		#distances = dict(nx.all_pairs_shortest_path_length(interactome))
		#print(distances)
		#sys.exit()		
		#d = nested_dict_max(distances)
		#print("Diameter: ", nested_dict_max(distances))

		#nx.draw(interactome)
		#print(nx.diameter(interactome))
		#plt.show()
		#sys.exit()
		#print("Distances: ", distances)
		
		subgraphs = sorted(list(nx.connected_component_subgraphs(interactome)), key=graphsize, reverse=True)
		interactome = subgraphs[0] # Pick largest connected component.
		#print([graphsize(g) for g in subgraphs])
		#sys.exit()
		# Greedy Method
		#tfd = greedy_tfd(interactome)
		
		# CBB Method.
		
		G_reduce = box_Graph(interactome)		
		tfd = tfd_burning(interactome, G_reduce, iterations=20, name=organism_name)
			
		#writer.writerow([organism_name, str(np.abs(tfd))])
		print("TFD: ", str(np.abs(tfd)))
		
		# SPEED UP BY COMPUTING DISTANCE AND DIAMETER ALL AT ONCE!
