from dist_colour import greedy_tfd, shuffle_labels
import networkx as nx
#import seaborn as sns
#import matplotlib.pyplot as plt
import numpy as np

NUM_RUNS = 10

from trial import box_covering, box_Graph, tfd_burning



def dimensions_run(graph, NUM_RUNS):
	dimensions = []
	for i in range(NUM_RUNS):
		# CBB Method.
		G_reduce = box_Graph(graph)		
		tfd = tfd_burning(graph, G_reduce)
		dimensions.append(tfd)

		#dimensions.append(greedy_tfd(graph))

		graph = shuffle_labels(graph)
	return dimensions

# TODO: Other graphs?

results = []
categories = []

graph = nx.fast_gnp_random_graph(10, 0.8)# ER graph epect larger TFD. (nodes, edge probability)
results.extend(dimensions_run(graph, NUM_RUNS))
categories.extend(["ER-10"] * NUM_RUNS)

"""
graph = nx.complete_graph(100)
results.extend(dimensions_run(graph, NUM_RUNS))
categories.extend(["Complete Graph"] * NUM_RUNS)
"""

graph = nx.circular_ladder_graph(100)
results.extend(dimensions_run(graph, NUM_RUNS))
categories.extend(["Circular Ladder Graph"] * NUM_RUNS)

graph = nx.convert_node_labels_to_integers(nx.grid_graph([2, 2]))
results.extend(dimensions_run(graph, NUM_RUNS))
categories.extend(["Grid Graph"] * NUM_RUNS)


graph = nx.path_graph(100)
results.extend(dimensions_run(graph, NUM_RUNS))
categories.extend(["Path Graph"] * NUM_RUNS)

"""
graph = nx.star_graph(100)
results.extend(dimensions_run(graph, NUM_RUNS))
categories.extend(["Star Graph"] * NUM_RUNS)
"""

np.save("categories.npy", categories)
np.save("results.npy", results)
"""
sns.set(style="whitegrid")
# X = category, Y = variable
ax = sns.violinplot(x=categories, y=results)
plt.show()
"""
