# -*- coding: utf-8 -*-
from __future__ import division
import networkx as nx
import os, sys, csv, re
import matplotlib.pyplot as plt
from collections import Counter
from network import build_graph
import numpy as np

powerlaw = lambda x, amp, index: amp * (x**index)

def main():
	limit = 50
	path = sys.argv[1]

	organism_name = re.findall("BIOGRID-ORGANISM-.+-[0-9]\.[0-9]+\.[0-9]+", path)[0]
	print("Processing: ", organism_name)	

	edgelist_path = os.path.join(path, organism_name + ".edgeList")
	symbol_path = os.path.join(path, organism_name + ".proteinSymbols")


	interactome = build_graph(edgelist_path, symbol_path)

	#1.3.3
	degrees = [value for key, value in interactome.degree().items()]
	counter = Counter(degrees)
	degree, count = zip(*counter.items())

	# Normalise
	count = np.array(count)
	count = count / np.max(count)
	
	# Fit a Power Law
	logdeg = np.log(degree)
	logcount = np.log(count)
	slope, intercept = np.polyfit(logdeg, logcount, 1)
	# Slope is power.
	

	print(list(zip(degree, count)))
	fig, ax = plt.subplots()
	plt.title("Degree Distribution")
	plt.ylabel("Count")
	plt.xlabel("Degree")
	#ax.set_yticks(list(range(max(count) + 1))) # Show all on y axis.
	
	plt.ylim(0, max(count))
	ax.set_xticks(degree)
	ax.set_xticklabels(degree, rotation='vertical')

	#plt.bar(degree[:50], count[:50], width=0.8, color="r", label="Degree Distribution")
	plt.bar(degree[:limit], count[:limit], label="Degree Distribution")	
	plt.plot(np.arange(0, len(degree[:limit]) + 1, 0.1), powerlaw(np.arange(0, len(degree[:limit]) + 1, 0.1), np.exp(intercept), slope), color="red", label="Fit (Power Law), gamma=" + "{:.2f}".format(slope))
	plt.legend()
	"""
	n, bins, patches = ax.hist(degree, 50, density=True, facecolor='red')
	#print(n)
	#ax.hist2d(degree, count)	
	"""	
	plt.show()

if __name__ == "__main__":
	main()
