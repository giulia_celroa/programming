"""
Analyse the running time data to compute the computational complexity.
"""
import pandas
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import pearsonr

# Read the data and convert to a 2D array with Pandas.
data = pandas.read_csv("complexity_data.csv")
dm = data.as_matrix(columns=["time_graph", "nodes", "edges"])

#test[column, rows]
log_time = np.log(dm[0,:])
log_nodes = np.log(dm[1,:])
log_edges = np.log(dm[2,:])

log_nodes_edges = np.log(dm[1,:] + dm[2,:])

# Log-Log Plots
fig = plt.figure()
ax = fig.add_subplot(211)
slope, intercept = np.polyfit(log_nodes, log_time, 1)
ax.text(0.1, 0.9, "Slope: " + '{0:.2f}'.format(slope), fontsize=10, transform=ax.transAxes)
plt.title("Log-Log: Nodes vs Running Time")
plt.xlabel("[Log] Nodes")
plt.ylabel("[Log] Running Time")
plt.loglog(dm[1,:], dm[0,:])

ax = fig.add_subplot(212)
slope, intercept = np.polyfit(log_edges, log_time, 1)
ax.text(0.1, 0.9, "Slope: " + '{0:.2f}'.format(slope), fontsize=10, transform=ax.transAxes)
plt.title("Log-Log: Edges vs Running Time")
plt.xlabel("[Log] Edges")
plt.ylabel("[Log] Running Time")
plt.loglog(dm[2,:], dm[0,:])
fig.tight_layout()

"""
ax = fig.add_subplot(313)
slope, intercept = np.polyfit(log_nodes_edges, log_time, 1)
ax.text(0.1, 0.9, "Slope: " + '{0:.2f}'.format(slope), fontsize=10, transform=ax.transAxes)
plt.title("Log-Log: Edges + Nodes vs Running Time")
plt.xlabel("[Log] Edges")
plt.ylabel("[Log] Running Time")
plt.loglog(dm[2,:] + dm[1,:], dm[0,:])
"""
fig.tight_layout()


plt.show()
