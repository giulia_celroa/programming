import networkx as nx
import os, sys, glob, csv, re
from dist_colour import greedy_tfd, _diameter
from network import build_graph
from trial import box_covering, box_Graph, tfd_burning
import numpy as np

path = sys.argv[1]
files = sorted(glob.glob(os.path.join(path, "*")))

sub_graphs = []
for file in files:
	try:
		#organism_name = re.findall("BIOGRID-ORGANISM-.+-[0-9]\.[0-9]\.[0-9]+",file)[0]		
		organism_name = re.findall("BIOGRID-ORGANISM-.+-[0-9]\.[0-9]\.[0-9]+", file)[0]
	except IndexError: # The folder doesn't match the name pattern!
		continue
	#print("Processing: ", organism_name)
	
	edgelist_path = os.path.join(file, organism_name + ".edgeList")
	symbol_path = os.path.join(file, organism_name + ".proteinSymbols")

	interactome = build_graph(edgelist_path, symbol_path)
	print(organism_name, ",", len(interactome.nodes()), ",", len(interactome.edges()))
	#for subg in nx.connected_components(interactome):
	#	print(len(subg))
	
	#sub_graphs.append(len(nx.connected_components(interactome)))
"""
import matplotlib.pyplot as plt

times = [str(t) for t in range(2006, 2018 + 1)]

fig = plt.figure()
ax = fig.add_subplot(111)
plt.title("Number of Connected Components")
plt.xlabel("Year")
plt.ylabel("Count")
ax.plot(times, sub_graphs, label="Connected Components")
ax.legend()
fig.tight_layout()
plt.show()

print(sub_graphs)
"""
