import networkx as nx
from network import build_graph
import numpy as np
import sys, csv
#import matplotlib.pyplot as plt
import networkx.algorithms
import pdb 

def dimension_from_stats(stats): 
	log_distances = np.log(stats[:, 0] + 1) # Prevent divide by 0 error in log (adds 1 to box size)
	log_boxes = np.log(stats[:, 1]) 

	slope, intercept = np.polyfit(log_distances, log_boxes, 1)
	# Positive of Slope is dimension.
	return np.abs(slope)

def get_colour(max_colour, used_colours):
	for colour in range(max_colour):
			if colour not in used_colours:
				return colour
	return max_colour

"""
Shuffle the labels in the graph to get different vertex orderings for colouring.
N.B. Currently requires int node IDs! Problem with tab1 format (TODO).
	@param graph The graph whose nodes IDs should be shuffled.
	@return The graph with the node IDs shuffled but edges preserved.
"""
def shuffle_labels(graph):
	# Relabel the nodes randomly.
	N = len(graph.nodes())
	node_ids = list(range(N))
	random_permutation = np.random.permutation(node_ids)
	mapping = dict(zip(node_ids, random_permutation))
	graph = nx.relabel_nodes(graph, mapping)
	
	return graph

def _diameter(graph):
	# Unlike NetworkX handle disconnected graphs and get the maximum diameter in the connected components of the graph.
	if nx.is_connected(graph):
		return nx.diameter(graph)
	else:
		diameters = [nx.diameter(g) for g in nx.connected_component_subgraphs(graph)]
		return max(diameters)

def nested_dict_max(dic):
    m = 0
    for _, d1 in dic.items():
        for k2, v in d1.items():
            if v > m:
                m = v
    return m

def nested_dict_min(dic):
    m = 100000000 # MAKE MAX INT
    for _, d1 in dic.items():
        for k2, v in d1.items():
            if v < m and v != 0:
                m = v
    return m

def _greedy_tfd_stats(graph):
	# Extract import graph information.
	N = len(graph.nodes())
	print("Graph size: ", N)
	# Compute all the distances in advance.
	distances = dict(nx.all_pairs_shortest_path_length(graph)) #nx.shortest_path_length(graph)
	
	# Speed up by getting diameter from distances matrix instead of re-computing (is max distance).	
	l_b_max = nested_dict_max(distances) # Speed up more, check return of diameter func.
	#min_dist = nested_dict_min(distances)
	#print("Min dist", min_dist)
	# l_b_max = nx.diameter(graph) #+ 1
	# l_b_max = _diameter(graph) #+ 1
	
	#print("Distances: ", distances)
	stats = []
	for l_b in range(0, l_b_max + 1):
		dual = nx.Graph()
		colouring = { 0:0 }
		used_colours = set()
		for i in distances.keys():#range(1, N):
			"""
			for j in range(i):
				print(i, j)
				pdb.set_trace()
				if distances[i][j] > l_b:
					#dual.add_edge(i, j)
					used_colours.add(colouring[j])
			"""
			for node, dist in distances[i].items():
				if dist > l_b:
					if (i == node):
						print("SELF LOOP??") #pass#print("PATH OF LENGTH 1")
					#dual.add_edge(i, node)
					try:
						used_colours.add(colouring[node])
					except KeyError:
						#used_colours.add(0)
						continue
			
			colouring[i] = get_colour(N, used_colours)
			#print(used_colours)
					#print("Link", (i, j))
		#plt.title("Dual Graph: " + str(l_b))
		#nx.draw(dual)	
		#plt.show()
		#print(colouring)
		#pdb.set_trace()
		print(l_b, len(np.unique(colouring.values())))
		stats.append((l_b, len(np.unique(colouring.values()))))

	return stats

def greedy_tfd(graph):
	return dimension_from_stats(np.array(_greedy_tfd_stats(graph)))

if __name__ == "__main__":
	# Random Graph (10 nodes, 80% chance of edge creation).
	#graph = nx.fast_gnp_random_graph(10, 0.3)# ER graph epect larger TFD. (nodes, edge probability)
	#graph = nx.grid_graph(dim=[2, 3, 4]) # Grid Lattice graph (expecteed dimension = 2)
	"""
	graph = nx.Graph()
	graph.add_edge(0, 2)
	graph.add_edge(0, 1)
	graph.add_edge(1, 2)
	graph.add_edge(2, 3)
	"""
	
	graph = nx.Graph()
	with open("cellular.dat", "rb") as file:
		for row in file:
			a, b = row.split(" ")
			graph.add_edge(int(a), int(b))
	""""""
	#graph = nx.complete_graph(50)
	#graph = nx.tutte_graph()
	#print("Nodes: ", N)
	#print("Diameter: ", l_b_max)
	stats = _greedy_tfd_stats(graph)
	np.save("stats.npy", stats)
	#print("Dimension: ", greedy_tfd(graph))
	print(dimension_from_stats(np.array(stats)))


