import networkx as nx
import os, sys, csv, re

from collections import Counter

"""
Build a networx Graph object from the edge list and symbol files.
	@param edgelist_path The path to the edge list file.
	@param symbol_path The path to the symbol file.
"""
def build_graph(edgelist_path, symbol_path):
	interactome = nx.Graph()

	with open(edgelist_path, "r") as file: #rb
		reader = csv.reader(file, delimiter=",", quotechar="\"")
		for row in reader:
			#interactome.add_edge(int(row[0]), int(row[1]))
			interactome.add_edge(row[0].strip(), row[1].strip())
		
	node_symbols = {}
	with open(symbol_path, "r") as file: #rb
		reader = csv.reader(file, delimiter=",", quotechar="\"")
		for row in reader:
			interactome.node[row[0].strip()]["symbol"] = row[1].strip()
			 #node_symbols[int(row[0])] = row[1]

	#return interactome
	# For the TFD etc to work need nodes to be labelled consecutively - name information still preserved in "symbol" property.
	
	return nx.convert_node_labels_to_integers(interactome)

"""
Give a dict of nodes: centrality scores return the top 5.
	@param centrality result A dict whose keys are node IDs and whose values are the centrality scores for each node.
"""
def top5_centrality(centrality_result):
	return sorted(centrality_result, key=centrality_result.get, reverse=True)[:5]

"""
Extract the official names of a protein or protein from the graph given the ID. These are stored in the node's "symbol" attribute.

	@param ints If ints then returns the symbol (official name) associated with that node in the graph. If [ints] then returns a list of symbols corresponding to the IDs in [ints] in the same order.
	@throw KeyError if any ID passed as ints does not correspond to a node in the graph.
"""
def symbolify(ints, graph):
	if type(ints) == type([]):
		return [graph.node[x]["symbol"] for x in ints]
	else:
		return graph.node[ints]["symbol"]

"""
Return the largest connected component of the graph.
"""
def filter_graph(graph):
	def graphsize(g):
		return len(g.nodes())
	subgraphs = sorted(list(nx.connected_component_subgraphs(graph)), key=graphsize, reverse=True)
	return subgraphs[0]

def main():
	import matplotlib.pyplot as plt
	path = sys.argv[1]

	organism_name = re.findall("BIOGRID-ORGANISM-.+-[0-9]\.[0-9]", path)[0]
	print("Processing: ", organism_name)	

	edgelist_path = os.path.join(path, organism_name + ".edgeList")
	symbol_path = os.path.join(path, organism_name + ".proteinSymbols")


	interactome = build_graph(edgelist_path, symbol_path)
	interactome = filter_graph(interactome)

	#nx.set_node_attributes(interactome, 'symbol', node_symbols)

	#interactome.add_node(row[BIOGRID_NAME_A], {"symbol": row[OFFICIAL_SYMBOL_A] })
	#interactome.add_node(row[BIOGRID_NAME_B], {"symbol": row[OFFICIAL_SYMBOL_B] })
	print("Number of Edges: ", len(interactome.edges()))
	print("Number of Nodes: ", len(interactome.nodes()))

	# TODO: test numbers match results from somewhere else.


	degree_c = nx.degree_centrality(interactome)
	between_c = nx.betweenness_centrality(interactome)
	eig_c = nx.closeness_centrality(interactome)

	#print(degree_c, between_c, eig_c)
	#print(symbolify(top5_centrality(degree_c), interactome))
	#print(symbolify(top5_centrality(between_c), interactome))
	#print(symbolify(top5_centrality(eig_c), interactome))

	#1.3.3
	degrees = [value for key, value in interactome.degree().items()]
	counter = Counter(degrees)
	degree, count = zip(*counter.items())

	print(degree)
	print(count)

	fig, ax = plt.subplots()
	plt.title("Degree Distribution")
	plt.ylabel("Count")
	plt.xlabel("Degree")
	ax.set_yticks(list(range(max(count) + 1))) # Show all on y axis.

	ax.set_xticks(degree)
	ax.set_xticklabels(degree)

	plt.bar(degree, count, width=0.7, color="r")

	plt.show()


	#labels = nx.get_node_attributes(interactome, 'symbol')
	#nx.draw(interactome, labels=labels, font_weight='bold')
	#plt.show()

if __name__ == "__main__":
	main()
