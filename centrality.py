import networkx as nx
import os, sys, csv, re
import matplotlib.pyplot as plt
from collections import Counter
from network import build_graph, filter_graph

"""
Give a dict of nodes: centrality scores return the top 5.
	@param centrality result A dict whose keys are node IDs and whose values are the centrality scores for each node.
"""
def top5_centrality(centrality_result):
	return sorted(centrality_result, key=centrality_result.get, reverse=True)[:5]

"""
Extract the official names of a protein or protein from the graph given the ID. These are stored in the node's "symbol" attribute.

	@param ints If ints then returns the symbol (official name) associated with that node in the graph. If [ints] then returns a list of symbols corresponding to the IDs in [ints] in the same order.
	@throw KeyError if any ID passed as ints does not correspond to a node in the graph.
"""
def symbolify(ints, graph):
	if type(ints) == type([]):
		return [graph.node[x]["symbol"] for x in ints]
	else:
		return graph.node[ints]["symbol"]

def main():
	path = sys.argv[1]

	organism_name = re.findall("BIOGRID-ORGANISM-.+-[0-9]\.[0-9]+\.[0-9]+", path)[0]
	print("Processing: ", organism_name)	

	edgelist_path = os.path.join(path, organism_name + ".edgeList")
	symbol_path = os.path.join(path, organism_name + ".proteinSymbols")

	interactome = build_graph(edgelist_path, symbol_path)
	interactome = filter_graph(interactome)

	print("Number of Edges: ", len(interactome.edges()))
	print("Number of Nodes: ", len(interactome.nodes()))

	#degree_c = nx.degree_centrality(interactome)
	between_c = nx.betweenness_centrality(interactome)
	#close_c = nx.closeness_centrality(interactome)
	
	#print(degree_c, between_c, eig_c)
	#print("Degree Centrality", symbolify(top5_centrality(degree_c), interactome))
	print("Betweenness Centrality", symbolify(top5_centrality(between_c), interactome))
	#print("Closeness Centrality", symbolify(top5_centrality(close_c), interactome))


if __name__ == "__main__":
	main()
